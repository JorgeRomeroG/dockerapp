FROM python
LABEL Jorge Romero
ENV HOME /root
RUN git clone https://gitlab.com/JorgeRomeroG/dockerapp.git
CMD python dockerapp/hello.py